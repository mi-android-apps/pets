# Pets
When a new pet comes into the shelter, you can add it to the list by clicking the Floating Action Button (FAB) in the bottom right corner.
When you click on a pet, you're taken to its detail page. You can update the pet’s information. 
If a pet is entered by mistake, they can be removed via the "Delete" menu option from the detail screen.


### Objectives
* Learn how to use SQLite, the database system that runs on Android
* Learn how to use SQLite within your Android App
* Learn the fundamentals of working with a ContentProvider
* Learn how the adapters and loaders move data from your ContentProvider to the user interface of your app.

### Build Instructions
This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

### screenshots
![please find images under app-screenshots directory](app-screenshots/screen-1.png "screen one")
![please find images under app-screenshots directory](app-screenshots/screen-2.png "screen two")
![please find images under app-screenshots directory](app-screenshots/screen-3.png "screen three")
![please find images under app-screenshots directory](app-screenshots/screen-4.png "screen four")
![please find images under app-screenshots directory](app-screenshots/screen-5.png "screen five")
![please find images under app-screenshots directory](app-screenshots/screen-6.png "screen six")
![please find images under app-screenshots directory](app-screenshots/screen-7.png "screen seven")
